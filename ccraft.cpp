#define _X_OPEN_SOURCE_EXTENDED

#include <locale.h>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <ncursesw/ncurses.h>

#define ICON_TREE	"↟"
#define ICON_GRASS	"⌴"

#define RUNE_F		"ᚠ"
#define RUNE_U		"ᚢ"
#define RUNE_M		"ᛗ"

#define DIR_RIGHT	0
#define DIR_DOWN	1
#define DIR_LEFT	2
#define DIR_UP		3

using namespace std;

void 						initCurses();
void						drawMap();
void						drawEntities();
void						act();
void						log( string message );

string						logMessage{"Game started."};

/* =============================================================================
 * Point describes single point in the gameworld. Point can be compared to other
 * Point with == and != operators.
 * ========================================================================== */
class Point {
public:
	int						x{1};
	int						y{1};
	
	Point() {}
	
	Point( const Point &p ) {
		x = p.x;
		y = p.y;
	}
	
	Point( const int x, const int y) {
		this->x = x;
		this->y = y;
	}
	
	void
	randomize ( int width, int height ) {
		x = rand()%(width-1)+1;
		y = rand()%(height-1)+1;
	}
	
	bool
	in( int px, int py, int width, int height) {
		if(x < px || y < py || x > width || y > height) {
			return false;
		}
		
		return true;
	}
	
	Point&
	operator=( const Point& other) {
		this->x = other.x;
		this->y = other.y;
		
		return *this;
	}
};

bool
operator==( const Point& a, const Point& b ) {
	return ( a.x == b.x && a.y == b.y );
}
	
bool
operator!=( const Point& a, const Point& b ) {
	return !(a==b);
}

/* =============================================================================
 * Frame is wrapper for ncurses WINDOW
 * ========================================================================== */

class Frame {
public:
	Frame( int x, int y, int width, int height, string title ) {
		_frame = newwin( height, width, y, x );
		//box(_frame, 0, 0);
		
		//wborder(_frame, '|', '|', '-','-','.','.','.','.');
		_title = title;
		draw();
	}
	
	void
	draw() {
		box(_frame, 0, 0);
		stringstream ss;
		ss << "┤ " << _title << " ├";
		mvwprintw(_frame, 0, 3, ss.str().c_str());
		wrefresh(_frame);
	}
	
	void
	mvprint( int y, int x, const char* msg) {
		mvwprintw(_frame, y, x, msg);
	}
private:
	WINDOW*					_frame;
	string 					_title;
};

/* =============================================================================
 * Item is something that Actors can carry in their inventories and use in
 * various tasks.
 * ========================================================================== */
class Item {
public:
	string					icon{">"};
	string					name{"-"};
	float					damage{0.0f};
	float					durability{1.0f};
	
	void
	makeLog() {
		name = "log";
		damage = 2.0f;
	}
};

/* =============================================================================
 * Entity is base class for all Entities in gameworld.
 * ========================================================================== */
const char TYPE_UNDEFINED 		= 1<<0;
const char TYPE_DAMAGEABLE 		= 1<<1;

class Entity {
public:
	Point					point;
	string					icon{"!"};
	string					name{"unknown"};
	float					health{10.0f};
	char					type{TYPE_UNDEFINED};
	
	bool					collides{false};
	
	void
	draw() {
		mvprintw(point.y, point.x, icon.c_str());
	}
	
	void
	drawInFrame(Frame* frame) {
		frame->mvprint(point.y, point.x, icon.c_str());
	}
	
	void
	makeTree() {
		icon = ICON_TREE;
		collides = true;
		type = TYPE_DAMAGEABLE;
		name = "tree";
	}
	
	bool damage( float hitPower ) {
		health -= hitPower;
		if( health <= 0 ) {
			return false;
		}
		
		return true;
	}
};

/* =============================================================================
 * Actor is a Entitycreature. Maybe needed, maybe not.
 * ========================================================================== */
class Actor: public Entity {
public:
	float					baseDamage{0.3f};
	Item*					weapon{0};
	
	Actor() {
		health = 12;
		name = "man";
	}
	
	string
	getWeaponString() {
		return "fist";
	}
	
	float
	getHitPower() {
		return baseDamage;
	}
	
	void
	addToInventory( Item item ) {
		inventory.push_back( item );
	}
private:
	vector<Item>			inventory;
};

/* =============================================================================
 * Local area. World constains many of these.
 * ========================================================================== */
class Area {
public:
	const int 		WIDTH = 35;
	const int 		HEIGHT = 13;
	Point			point;
	vector<Entity>	data;
	
	Area()  {
		int random;
		_generate();
		
		point.x = 3; 
		point.y = 3;
	}
	
	Area( int x, int y )  {
		_generate();
		
		point.x = x; 
		point.y = y;
	}
	
	void
	draw(Frame* frame) {
		for(int y = 1; y < HEIGHT; y++) {
			for(int x = 1; x < WIDTH; x++) {
				frame->mvprint( y, x, ICON_GRASS);
			}
		}
		
		for(Entity e : data) {
			e.drawInFrame(frame);
		}
	}
	
	Entity*
	at( const Point& point ) {
		for( int i = 0; i < data.size(); i++ ) {
			if( point == data[i].point ) {
				return &data[i];
			}
		}
		
		return NULL;
	}
	
	bool
	isPassable( const Point& point ) {
		Entity* e = at(point);
		
		if( !e ) {
			return true;
		}
		
		if( e->collides ) {
			return false;
		}
		
		
		return true;
	}
	
	void
	remove( Entity* e) {
		for( int i = 0; i < data.size(); i++ ) {
			if( &data[i] == e ) {
				data.erase(data.begin()+i);
			}
		}
	}
private:
	void
	_generate() {
		int random = rand()%30+3;
		for( int i = 0; i < random; i++ ) {
			Entity e;
			e.makeTree();
			e.point.randomize(WIDTH-3, HEIGHT-3);
			e.point.x += 1;
			e.point.y += 1;
			data.push_back(e);
		}
	}
};

/* =============================================================================
 * World contains several areas.
 * ========================================================================== */
class World{
public:
	const int				WIDTH{20};
	const int				HEIGHT{13};
	vector<Area*>			areas;
	Area*					currentArea;
	
	World() {
		Area* a = _createArea( 3, 3 );
		currentArea = a;
	}
	
	void
	draw(Frame* frame) {
		for(int i = 0; i < areas.size(); i++) {
			string icon = ".";
			if( areas[i]->point == currentArea->point ) {
				icon = RUNE_M;
			}
			
			frame->mvprint(areas[i]->point.y, areas[i]->point.x, icon.c_str() );
		}
	}
	
	bool
	move( char dir) {
		Point point(currentArea->point);
		if( dir == DIR_UP) {
			if( point.y > 1 ) {
				currentArea = _createArea( point.x, point.y-1);
				return true;
			}
		}
		
		else if( dir == DIR_LEFT ) {
			if( point.x > 1 ) {
				currentArea = _createArea( point.x-1, point.y);
				return true;
			}
		}
		
		else if( dir == DIR_DOWN ) {
			if( point.y < currentArea->HEIGHT-1 ) {
				currentArea = _createArea( point.x, point.y+1);
				return true;
			}
		}
		
		else if( dir == DIR_RIGHT ) {
			if( point.x < currentArea->WIDTH-2 ) {
				currentArea = _createArea( point.x+1, point.y);
				return true;
			}
		}
		
		return false;
	}
private:
	Area*
	_createArea( int x, int y ) {
		for(Area* a : areas) {
			if(a->point == Point(x, y)) {
				return a;
			}
		}
		
		Area* a = new Area();
		a->point.x = x;
		a->point.y = y;
		areas.push_back(a);
		return a;
	}
};

char					in;
Actor					player;
Area*					area;
World*					world;
Frame*					playFrame;
Frame*					worldMap;

int
main ( int argc, char **argv) {
	// System initialization.
	setlocale(LC_CTYPE,"");
	srand(time(0));
	
	// ncurses initialization.
	initCurses();
	
	// Rune ᛗ represents man.
	player.icon = RUNE_M;
	
	// Create new world. Initial area is also created.
	world = new World();
	
	// Fetch pointer to initial area from world.
	area = world->currentArea;
	
	// Frame in the left. Local map.
	playFrame = new Frame(3, 3, area->WIDTH+1, area->HEIGHT+1, "❖ local map");
	
	// Frame in right. World map.
	worldMap = new Frame(40, 3, world->WIDTH+1, world->HEIGHT+1, "❖ world map");
	
	
	// Main loop
	while(in != 'q') {
		clear();
		
		drawMap();
		drawEntities();
		refresh();
		
		playFrame->draw();
		worldMap->draw();
		
		stringstream s;
		s << "log: " << logMessage;
		mvprintw(1,2, s.str().c_str());
		
		in = getch();
		act();
	}
	
	endwin();
	return 0;
}

void
initCurses() {
	initscr();
	//raw();
	curs_set(0);
	keypad(stdscr, TRUE);
	noecho();
	cbreak();
}

void
drawMap() {
	area->draw(playFrame);
	world->draw(worldMap);
}

void
drawEntities() {
	player.drawInFrame(playFrame);
}

void
act() {
	Point oldPoint(player.point);
	//Movement
	if ( in == 'a' ) {
		player.point.x -= 1;
	}
	
	else if ( in == 'd' ) {
		player.point.x += 1;
	}
	
	else if ( in == 'w' ) {
		player.point.y -= 1;
	}
	
	else if ( in == 's' ) {
		player.point.y += 1;
	}
	
	Entity* e = area->at(player.point);
	if( e && e->type == TYPE_DAMAGEABLE ) {
		if (!e->damage(player.getHitPower())) {
			stringstream s;
			s << "The " << e->name << " fell.";
			log(s.str());
			
			area->remove(e);
		}
		
		else {
			stringstream s;
			s << player.name << " hits a " << e->name << " with " << player.getWeaponString() << ". " << "(" << e->health << " hp remaining)";
			log(s.str());
		}
	}
	
	if( !player.point.in(1, 1, area->WIDTH-1, area->HEIGHT-1) ) {
		// Left
		if( player.point.x < 1 && world->move( DIR_LEFT )) {
			player.point.x = area->WIDTH-1;
			area = world->currentArea;
		}
		
		// Up
		else if( player.point.y < 1 && world->move( DIR_UP )) {
			player.point.y = area->HEIGHT-1;
			area = world->currentArea;
		}
		
		// Right
		else if( player.point.x == area->WIDTH && world->move( DIR_RIGHT )) {
			player.point.x = 1;
			area = world->currentArea;
		}
		
		// Down
		else if( player.point.y == area->HEIGHT && world->move( DIR_DOWN )) {
			player.point.y = 1;
			area = world->currentArea;
		}
		
		else {
			player.point = oldPoint;
		}
	}
	
	if( !area->isPassable(player.point)) {
		player.point = oldPoint;
	}
}

void
log( string message) {
	logMessage = message;
}
