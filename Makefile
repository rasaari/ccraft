CC := g++
LDFLAGS := -lncursesw -Os
CFLAGS := -std=c++11

all: linux

linux:
	$(CC) $(CFLAGS) ccraft.cpp -o ccraft-linux $(LDFLAGS)
